unit FMX.TU2Signature;

interface

uses
  System.Classes, System.Types, System.UITypes,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Objects, FMX.TU2;

type
  IBezierPath = interface
    procedure MoveTo(const P: TPointF);
    procedure LineTo(const P: TPointF);                              //一阶
    procedure QuadTo(const ControlPoint, EndPoint: TPointF);         //二阶
    procedure CurveTo(const Control1, Control2, EndPoint: TPointF);  //三阶
    procedure DrawToBitmap(const ACanvas: TCanvas);
    procedure Clear;
    function IsEmpty: Boolean;
    {Update}
    procedure Resize(const AWidth, AHeight: Single);
    procedure SetPenColor(const Value: TAlphaColor);
    procedure SetPenThickness(const Value: Single);
  end;

  [ComponentPlatformsAttribute(TU2FMXPlatforms)]
  TSignature = class(TRectangle)
  private
    FPrevPoint: TPointF;
    FPath: IBezierPath;
    FIndex: Integer;
    FPenColor: TAlphaColor;
    FPenThickness: Single;
  private
    procedure SetPenColor(const Value: TAlphaColor);
    procedure SetPenThickness(const Value: Single);
    function GetEmpty: Boolean;
  protected
    procedure CMGesture(var EventInfo: TGestureEventInfo); override;
    procedure Paint; override;
    procedure DoResized; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Clear;
    property Empty: Boolean read GetEmpty;
  published
    property PenColor: TAlphaColor read FPenColor write SetPenColor;
    property PenThickness: Single read FPenThickness write SetPenThickness;
  end;

implementation

uses
{$IFDEF IOS}
  FMX.TU2Signature.iOS,
{$ENDIF IOS}
{$IFDEF ANDROID}
  FMX.TU2Signature.Android,
{$ENDIF}
  System.SysUtils;

{ TSignatureControl }

constructor TSignature.Create(AOwner: TComponent);
begin
  inherited;
  {$IFDEF IOS}
  FPath := TiOSBezierPath.Create;
  {$ENDIF}
  {$IFDEF ANDROID}
  FPath := TAndroidBezierPath.Create;
  {$ENDIF}
  FPenColor := TAlphaColorRec.Black;
  FPenThickness := 2;
  Touch.InteractiveGestures := [TInteractiveGesture.Pan];
end;

procedure TSignature.DoResized;
begin
  inherited;
  {$IF Defined(IOS) OR Defined(ANDROID)}
  FPath.Resize(Width, Height);
  {$ENDIF}
end;

function TSignature.GetEmpty: Boolean;
begin
  {$IF Defined(IOS) OR Defined(ANDROID)}
  Result := FPath.IsEmpty;
  {$ELSE}
  Result := True;
  {$ENDIF}
end;

procedure TSignature.Paint;
begin
  inherited;
  {$IF Defined(IOS) OR Defined(ANDROID)}
  if not FPath.IsEmpty then
    FPath.DrawToBitmap(Canvas);
  {$ENDIF}
end;

procedure TSignature.SetPenColor(const Value: TAlphaColor);
begin
  {$IF Defined(IOS) OR Defined(ANDROID)}
  FPath.SetPenColor(Value);
  {$ENDIF}
  FPenColor := Value;
end;

procedure TSignature.SetPenThickness(const Value: Single);
begin
  {$IF Defined(IOS) OR Defined(ANDROID)}
  FPath.SetPenThickness(Value);
  {$ENDIF}
  FPenThickness := Value;
end;

procedure TSignature.Clear;
begin
  FPath.Clear;
  Repaint;
end;

procedure TSignature.CMGesture(var EventInfo: TGestureEventInfo);
var
  LP: TPointF;
begin
  if EventInfo.GestureID=igiPan then
  begin
    LP := AbsoluteToLocal(EventInfo.Location);
    if PointInObjectLocal(LP.X,LP.Y) then
    begin
      if TInteractiveGestureFlag.gfBegin in EventInfo.Flags then
      begin
        FPath.MoveTo(LP);
        FIndex := 0;
        FPrevPoint := LP;
      end else if EventInfo.Flags=[] then
      begin
        if FIndex=0 then
          Inc(FIndex)
        else begin
          FPath.QuadTo(FPrevPoint, LP.MidPoint(FPrevPoint));
          Repaint;
        end;
        FPrevPoint := LP;
      end else if TInteractiveGestureFlag.gfEnd in EventInfo.Flags then
      begin
        FPath.QuadTo(FPrevPoint, LP.MidPoint(FPrevPoint));
        Repaint;
      end;
    end;
  end else
    inherited;
end;

end.
