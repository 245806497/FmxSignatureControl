unit FMX.TU2Signature.iOS;

interface

uses System.Types, System.UITypes, FMX.Graphics, FMX.TU2Signature, iOSapi.UIKit;

type
  TiOSBezierPath = class(TInterfacedObject, IBezierPath)
  private
    FData: UIBezierPath;
    FPenColor: TAlphaColor;
    FSize: TSize;
  protected
    procedure MoveTo(const P: TPointF);
    procedure LineTo(const P: TPointF);                              //一阶
    procedure QuadTo(const ControlPoint, EndPoint: TPointF);         //二阶
    procedure CurveTo(const Control1, Control2, EndPoint: TPointF);  //三阶
    procedure DrawToBitmap(const ACanvas: TCanvas);
    procedure Clear;
    function IsEmpty: Boolean;
    {Update}
    procedure Resize(const AWidth, AHeight: Single);
    procedure SetPenColor(const Value: TAlphaColor);
    procedure SetPenThickness(const Value: Single);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses iOSapi.Foundation, iOSapi.CoreGraphics, iOSapi.CocoaTypes, FMX.Helpers.iOS;

{ TiOSBezierPath }

constructor TiOSBezierPath.Create;
begin
  FData := TUIBezierPath.Wrap(TUIBezierPath.OCClass.bezierPath);
  FData.setLineWidth(2);
  FData.setLineCapStyle(kCGLineCapRound);
  FData.setLineJoinStyle(kCGLineJoinRound);
  FData.retain;
  FPenColor := TAlphaColorRec.Black;
end;

destructor TiOSBezierPath.Destroy;
begin
  FData.release;
  inherited;
end;

procedure TiOSBezierPath.Clear;
begin
  FData.removeAllPoints;
end;

function TiOSBezierPath.IsEmpty: Boolean;
begin
  Result := FData.isEmpty;
end;

procedure TiOSBezierPath.MoveTo(const P: TPointF);
begin
  FData.moveToPoint(NSPoint.Create(P));
end;

procedure TiOSBezierPath.LineTo(const P: TPointF);
begin
  FData.addLineToPoint(NSPoint.Create(P));
end;

procedure TiOSBezierPath.QuadTo(const ControlPoint, EndPoint: TPointF);
begin
  FData.addQuadCurveToPoint(NSPoint.Create(EndPoint),NSPoint.Create(ControlPoint));
end;

procedure TiOSBezierPath.CurveTo(const Control1, Control2, EndPoint: TPointF);
begin
  FData.addCurveToPoint(NSPoint.Create(EndPoint),NSPoint.Create(Control1),NSPoint.Create(Control2));
end;

procedure TiOSBezierPath.DrawToBitmap(const ACanvas: TCanvas);
var
  img: UIImage;
  bm: TBitmap;
begin
  UIGraphicsBeginImageContextWithOptions(CGSizeMake(FSize.Width, FSize.Height), False, 0);
  //绘制曲线
  AlphaColorToUIColor(FPenColor).setStroke;
  FData.stroke;
  //GPU绘制缓存
  img := TUIImage.Wrap(UIGraphicsGetImageFromCurrentImageContext);
  bm := UIImageToBitmap(img, 0, FSize);
  UIGraphicsEndImageContext;
  ACanvas.DrawBitmap(bm, bm.BoundsF, TRectF.Create(0,0,FSize.cx, FSize.cy), 1);
end;

procedure TiOSBezierPath.SetPenColor(const Value: TAlphaColor);
begin
  FPenColor := Value;
end;

procedure TiOSBezierPath.SetPenThickness(const Value: Single);
begin
  FData.setLineWidth(Value);
end;

procedure TiOSBezierPath.Resize(const AWidth, AHeight: Single);
begin
  FSize.cx := Round(AWidth);
  FSize.cy := Round(AHeight);
end;


end.
