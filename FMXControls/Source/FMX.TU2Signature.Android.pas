unit FMX.TU2Signature.Android;

interface

uses System.Types, System.UITypes, FMX.Graphics, FMX.TU2Signature,
  Androidapi.JNI.GraphicsContentViewText;

type
  TAndroidBezierPath = class(TInterfacedObject, IBezierPath)
  private
    FData: JPath;
    FCanvas: JCanvas;
    FBitmap: JBitmap;
    FPaint: JPaint;
  protected
    procedure MoveTo(const P: TPointF);
    procedure LineTo(const P: TPointF);                              //一阶
    procedure QuadTo(const ControlPoint, EndPoint: TPointF);         //二阶
    procedure CurveTo(const Control1, Control2, EndPoint: TPointF);  //三阶
    procedure DrawToBitmap(const ACanvas: TCanvas);
    procedure Clear;
    function IsEmpty: Boolean;
    {Update}
    procedure Resize(const AWidth, AHeight: Single);
    procedure SetPenColor(const Value: TAlphaColor);
    procedure SetPenThickness(const Value: Single);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses System.SysUtils, FMX.Surfaces, FMX.Helpers.Android;

{ TAndroidBezierPath }

constructor TAndroidBezierPath.Create;
begin
  FData := TJPath.JavaClass.Init;
  FPaint := TJPaint.Wrap(TJPaint.JavaClass.init(TJPaint.JavaClass.ANTI_ALIAS_FLAG));
  FPaint.setStyle(TJPaint_Style.Wrap(TJPaint_Style.JavaClass.STROKE));
  FPaint.setStrokeCap(TJPaint_Cap.JavaClass.ROUND);
  FPaint.setStrokeJoin(TJPaint_Join.JavaClass.ROUND);
  FPaint.setStrokeWidth(2);
  FPaint.setColor(TAlphaColorRec.Black);
end;

destructor TAndroidBezierPath.Destroy;
begin
  FData := nil;
  FPaint := nil;
  FCanvas := nil;
  FBitmap := nil;
  inherited;
end;

procedure TAndroidBezierPath.Clear;
begin
  FData.reset;
end;

function TAndroidBezierPath.IsEmpty: Boolean;
begin
  Result := FData.isEmpty;
end;

procedure TAndroidBezierPath.MoveTo(const P: TPointF);
begin
  FData.moveTo(P.X, P.Y);
end;

procedure TAndroidBezierPath.LineTo(const P: TPointF);
begin
  FData.lineTo(P.X, P.Y);
end;

procedure TAndroidBezierPath.QuadTo(const ControlPoint, EndPoint: TPointF);
begin
  FData.quadTo(ControlPoint.X, ControlPoint.Y, EndPoint.X, EndPoint.Y);
end;

procedure TAndroidBezierPath.CurveTo(const Control1, Control2, EndPoint: TPointF);
begin
  FData.cubicTo(Control1.X, Control1.Y, Control2.X, Control2.Y, EndPoint.X, EndPoint.Y);
end;

procedure TAndroidBezierPath.DrawToBitmap(const ACanvas: TCanvas);
var
  Surface: TBitmapSurface;
  bm: TBitmap;
begin
  FCanvas.drawColor(0,TJPorterDuff_Mode.Wrap(TJPorterDuff_Mode.JavaClass.CLEAR));
  FCanvas.drawPath(FData, FPaint);
  //获取结果
  Surface := TBitmapSurface.Create;
  try
    if JBitmapToSurface(FBitmap, Surface) then
    begin
      bm := TBitmap.Create;
      bm.Assign(Surface);
      ACanvas.DrawBitmap(bm, bm.BoundsF, TRectF.Create(0,0,FBitmap.getWidth, FBitmap.getHeight), 1);
    end;
  finally
    Surface.Free;
  end;
end;

procedure TAndroidBezierPath.SetPenColor(const Value: TAlphaColor);
begin
  FPaint.setColor(Value);
end;

procedure TAndroidBezierPath.SetPenThickness(const Value: Single);
begin
  FPaint.setStrokeWidth(Value);
end;

procedure TAndroidBezierPath.Resize(const AWidth, AHeight: Single);
begin
  if FBitmap<>nil then
  begin
    FCanvas := nil;
    FBitmap.recycle;
    FBitmap := nil;
  end;
  FBitmap := TJBitmap.JavaClass.createBitmap(Round(AWidth), Round(AHeight),
    TJBitmap_Config.JavaClass.ARGB_8888);
  FCanvas := TJCanvas.JavaClass.init(FBitmap);
end;

end.
